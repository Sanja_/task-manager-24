package ru.karamyshev.taskmanager.endpoint;

import javax.xml.ws.WebServiceException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.karamyshev.taskmanager.api.IServiceLocator;
import ru.karamyshev.taskmanager.bootstrap.Bootstrap;
import ru.karamyshev.taskmanager.marker.ClientTestCategory;

import java.util.ArrayList;
import java.util.List;

@Category(ClientTestCategory.class)
public class UserEndpointTest {

    private final IServiceLocator serviceLocator = new Bootstrap();

    @Before
    public void init() throws java.lang.Exception {
        final UserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
        final User admin = userEndpoint.createUserRole("admin", "admin", Role.ADMIN);
        final List<User> userList = new ArrayList<>();
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        final User userTest = serviceLocator.getUserEndpoint().createUser("user-1", "user-1");
        final User userTestTwo = serviceLocator.getUserEndpoint().createUser("user-2", "user-2");
        userList.add(userTest);
        userList.add(userTestTwo);
        userList.add(admin);
        userEndpoint.loadUser(userList, session);
        Assert.assertEquals(userEndpoint.getUserList(session).size(), 3);
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void createUserTest() throws java.lang.Exception {
        final UserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
        userEndpoint.createUserRole("admin", "admin", Role.ADMIN);
        final Session session = serviceLocator.getSessionEndpoint().openSession("admin", "admin");
        final int countUsers = userEndpoint.getUserList(session).size();
        final User user = userEndpoint.createUser("user", "user");
        Assert.assertNotNull(user);
        Assert.assertEquals(userEndpoint.getUserList(session).size(), countUsers + 1);
        userEndpoint.removeUserById(session);
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void createUserEmailTest() throws java.lang.Exception {
        final UserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
        userEndpoint.createUserRole("admin", "admin", Role.ADMIN);
        final Session session = serviceLocator.getSessionEndpoint().openSession("admin", "admin");
        final int countUsers = userEndpoint.getUserList(session).size();
        final User user = userEndpoint.createUserEmail("user", "user", "user@mail");
        Assert.assertNotNull(user);
        Assert.assertEquals(userEndpoint.getUserList(session).size(), countUsers + 1);
        userEndpoint.removeUserById(session);
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void createUserRoleTest() throws java.lang.Exception {
        final UserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
        userEndpoint.createUser("admin", "admin");
        final Session session = serviceLocator.getSessionEndpoint().openSession("admin", "admin");
        final int countUsers = userEndpoint.getUserList(session).size();
        final User user = userEndpoint.createUserRole("userRole", "userRole", Role.USER);
        Assert.assertNotNull(user);
        Assert.assertEquals(userEndpoint.getUserList(session).size(), countUsers + 1);
        userEndpoint.removeUserById(session);
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void findUserByIdTest() throws java.lang.Exception {
        final UserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
        userEndpoint.createUserRole("admin", "admin", Role.ADMIN);
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        final User user = userEndpoint.findUserById(session);
        Assert.assertNotNull(user);
        Assert.assertThrows(WebServiceException.class, () -> userEndpoint.findUserById(null));
        userEndpoint.removeUserById(session);
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void findUserByLoginTest() throws Exception_Exception {
        final UserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
        userEndpoint.createUserRole("admin", "admin", Role.ADMIN);
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        final int countUsers = userEndpoint.getUserList(session).size();
        userEndpoint.createUser("user", "user");
        Assert.assertEquals(userEndpoint.getUserList(session).size(), countUsers + 1);
        final User user = userEndpoint.findUserByLogin(session, "user");
        Assert.assertNotNull(user);
        Assert.assertThrows(WebServiceException.class,
                () -> userEndpoint.findUserByLogin(null, "user"));
        userEndpoint.removeUserByLogin(session, "user");
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void removeUserByIdTest() throws Exception_Exception {
        final UserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
        userEndpoint.createUser("user", "user");
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("user", "user");
        final int countUsers = userEndpoint.getUserList(session).size();
        userEndpoint.removeUserById(session);
        Assert.assertEquals(userEndpoint.getUserList(session).size(), countUsers - 1);
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void removeByLoginTest() throws Exception_Exception {
        final UserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
        userEndpoint.createUser("user", "user");
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        final int countUsers = userEndpoint.getUserList(session).size();
        userEndpoint.removeByLogin(session, "user");
        Assert.assertEquals(userEndpoint.getUserList(session).size(), countUsers - 1);
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void removeUserByLoginTest() throws Exception_Exception {
        final UserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
        userEndpoint.createUserRole("admin", "admin", Role.ADMIN);
        userEndpoint.createUser("user", "user");
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        Assert.assertNotNull(session);
        final int countUsers = userEndpoint.getUserList(session).size();
        userEndpoint.removeUserByLogin(session, "user");
        Assert.assertEquals(userEndpoint.getUserList(session).size(), countUsers - 1);
        Assert.assertThrows(WebServiceException.class,
                () -> userEndpoint.removeUserByLogin(session, "admin"));
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void lockUserByLoginTest() throws Exception_Exception {
        final UserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
        userEndpoint.createUserRole("admin", "admin", Role.ADMIN);
        userEndpoint.createUser("user", "user");
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        final User user = userEndpoint.findUserById(session);
        Assert.assertNotNull(user);
        Assert.assertThrows(WebServiceException.class,
                () -> userEndpoint.lockUserByLogin(session, user.getLogin(), user.getLogin()));
        userEndpoint.lockUserByLogin(session, user.getLogin(), "user");
        final User userLock = userEndpoint.findUserByLogin(session, "user");
        Assert.assertTrue(userLock.isLocked());
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void unlockUserByLoginTest() throws Exception_Exception {
        final UserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");

        final User userTemp = userEndpoint.findUserById(session);
        userEndpoint.lockUserByLogin(session, userTemp.getLogin(), "user-1");
        Assert.assertTrue(userEndpoint.findUserByLogin(session, "user-1").isLocked());
        userEndpoint.unlockUserByLogin(session, "user-1");
        Assert.assertFalse(userEndpoint.findUserByLogin(session, "user-1").isLocked());
        serviceLocator.getSessionEndpoint().closeSession(session);
        serviceLocator.getSessionService().clearSession();
    }

    @Test
    public void loadUserTest() throws Exception_Exception {
        final UserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
        userEndpoint.createUserRole("admin", "admin", Role.ADMIN);
        final List<User> userList = new ArrayList<>();
        final User user1 = new User();
        final User user2 = new User();

        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        user1.setLogin("user-1");
        user1.setPasswordHash("user-11111111");
        user2.setLogin("user-2");
        user2.setPasswordHash("user-22222222");
        userList.add(user1);
        userList.add(user2);
        userEndpoint.loadUser(userList, session);
        Assert.assertEquals(userEndpoint.getUserList(session).size(), 2);
    }

}

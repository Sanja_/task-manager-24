package ru.karamyshev.taskmanager.endpoint;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.karamyshev.taskmanager.api.IServiceLocator;
import ru.karamyshev.taskmanager.bootstrap.Bootstrap;
import ru.karamyshev.taskmanager.marker.ClientTestCategory;

import java.util.ArrayList;
import java.util.List;

@Category(ClientTestCategory.class)
public class ProjectEndpointTest {

    private final IServiceLocator serviceLocator = new Bootstrap();

    @Before
    public void init() throws java.lang.Exception {
        final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        final UserEndpoint userEndpoint = serviceLocator.getUserEndpoint();

        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");

        final List<Project> projectList = new ArrayList<>();
        final Project project = new Project();
        project.setName("projectDemo");
        project.setDescription("projectDemo");
        project.setUserId(session.getUserId());
        project.setId(111111111L);
        projectList.add(project);
        projectEndpoint.loadProject(session, projectList);
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void createNameProjectTest() throws java.lang.Exception {
        final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        Assert.assertNotNull(session);
        final int amountProject = projectEndpoint.getProjectList(session).size();
        projectEndpoint.createNameProject(session, "project-1");
        Assert.assertEquals(projectEndpoint.getProjectList(session).size(), amountProject + 1);
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void createDescriptionProjectTest() throws java.lang.Exception {
        final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        Assert.assertNotNull(session);
        final int amountProject = projectEndpoint.getProjectList(session).size();
        projectEndpoint.createDescriptionProject(session, "project-1", "project-1");
        Assert.assertEquals(projectEndpoint.getProjectList(session).size(), amountProject + 1);
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void addProjectTest() throws java.lang.Exception {
        final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        Assert.assertNotNull(session);
        final Project project = new Project();
        project.setName("project-1");
        project.setId(111111L);
        final int amountProject = projectEndpoint.getProjectList(session).size();
        projectEndpoint.addProject(session, project);
        Assert.assertEquals(projectEndpoint.getProjectList(session).size(), amountProject + 1);
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void getProjectListTest() throws java.lang.Exception {
        final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        final int amountProject = projectEndpoint.getProjectList(session).size();
        projectEndpoint.createNameProject(session, "project-1");
        projectEndpoint.createNameProject(session, "project-2");
        projectEndpoint.createNameProject(session, "project-3");
        Assert.assertNotNull(session);
        Assert.assertEquals(projectEndpoint.getProjectList(session).size(), amountProject + 3);
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void findAllProjectTest() throws java.lang.Exception {
        final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        final int amountProject = projectEndpoint.getProjectList(session).size();
        projectEndpoint.createNameProject(session, "project-1");
        projectEndpoint.createNameProject(session, "project-2");
        projectEndpoint.createNameProject(session, "project-3");
        Assert.assertEquals(projectEndpoint.findAllProject(session).size(), amountProject + 3);
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void findOneProjectByIndexTest() throws java.lang.Exception {
        final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        projectEndpoint.createNameProject(session, "project-1");
        final Project project = projectEndpoint.findOneProjectByIndex(session, 2);
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getName(), "project-1");
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void findOneProjectByNameTest() throws java.lang.Exception {
        final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        projectEndpoint.createNameProject(session, "project-1");
        final List<Project> projectList = projectEndpoint.findOneProjectByName(session, "project-1");
        Assert.assertNotNull(projectList);
        Assert.assertEquals(1, projectList.size());
        Assert.assertEquals(projectList.get(0).getName(), "project-1");
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void findOneProjectByIdTest() throws java.lang.Exception {
        final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        projectEndpoint.createNameProject(session, "project-1");
        final List<Project> projects = projectEndpoint.findOneProjectByName(session,
                "project-1");
        final Project project = projectEndpoint.findOneProjectById(session,
                String.valueOf(projects.get(0).getId()));
        Assert.assertNotNull(String.valueOf(projects.get(0).getId()), project.getId());
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void clearProjectTest() throws java.lang.Exception {
        final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        final int amountProject = projectEndpoint.getProjectList(session).size();
        projectEndpoint.createNameProject(session, "project-1");
        projectEndpoint.createNameProject(session, "project-2");
        projectEndpoint.createNameProject(session, "project-3");
        Assert.assertEquals(projectEndpoint.findAllProject(session).size(), amountProject + 3);
        projectEndpoint.clearProject(session);
        Assert.assertEquals(projectEndpoint.findAllProject(session).size(), 0);
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void updateProjectByIdTest() throws java.lang.Exception {
        final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        projectEndpoint.createNameProject(session, "project-1");
        final Project project = projectEndpoint.findOneProjectByIndex(session, 1);
        Assert.assertNotNull(project);
        final Project projectUpdate = projectEndpoint.updateProjectById(session, String.valueOf(project.getId()),
                "project-11", "project-11");
        Assert.assertEquals(projectUpdate.getName(), "project-11");
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void updateProjectByIndexTest() throws java.lang.Exception {
        final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        projectEndpoint.createNameProject(session, "project-1");
        final Project project = projectEndpoint.findOneProjectByIndex(session, 1);
        Assert.assertNotNull(project);
        final Project projectUpdate = projectEndpoint.updateProjectByIndex(session, 1,
                "project-11", "project-11");
        Assert.assertNotNull(projectUpdate);
        Assert.assertEquals(projectUpdate.getName(), "project-11");
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void removeOneProjectByIndexTest() throws java.lang.Exception {
        final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        final int projectSize = projectEndpoint.getProjectList(session).size();
        projectEndpoint.createNameProject(session, "project-1");
        projectEndpoint.createNameProject(session, "project-2");
        projectEndpoint.createNameProject(session, "project-3");
        Assert.assertEquals(projectEndpoint.getProjectList(session).size(), projectSize + 3);
        projectEndpoint.removeOneProjectByIndex(session, 1);
        Assert.assertEquals(projectEndpoint.getProjectList(session).size(), projectSize + 2);
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void removeOneProjectByNameTest() throws java.lang.Exception {
        final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        final int projectSize = projectEndpoint.getProjectList(session).size();
        projectEndpoint.createNameProject(session, "project-1");
        projectEndpoint.createNameProject(session, "project-2");
        projectEndpoint.createNameProject(session, "project-3");
        Assert.assertEquals(projectEndpoint.getProjectList(session).size(), projectSize + 3);
        projectEndpoint.removeOneProjectByName(session, "project-1");
        Assert.assertEquals(projectEndpoint.
                findOneProjectByName(session, "project-1").size(), 0);
        Assert.assertEquals(projectEndpoint.getProjectList(session).size(), projectSize + 2);
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    public void removeOneProjectByIdTest() throws java.lang.Exception {
        final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        final Session session = serviceLocator.getSessionEndpoint()
                .openSession("admin", "admin");
        final int projectSize = projectEndpoint.getProjectList(session).size();
        projectEndpoint.createNameProject(session, "project-1");
        projectEndpoint.createNameProject(session, "project-2");
        projectEndpoint.createNameProject(session, "project-3");
        final Project project = projectEndpoint.findOneProjectByIndex(session, 1);
        Assert.assertNotNull(project);
        Assert.assertEquals(projectEndpoint.getProjectList(session).size(), projectSize + 3);
        projectEndpoint.removeOneProjectById(session, String.valueOf(project.getId()));
        Assert.assertEquals(projectEndpoint.getProjectList(session).size(), projectSize + 2);
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

}

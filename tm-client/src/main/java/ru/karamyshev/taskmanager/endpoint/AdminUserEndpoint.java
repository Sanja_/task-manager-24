package ru.karamyshev.taskmanager.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-02-15T18:26:24.785+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://endpoint.taskmanager.karamyshev.ru/", name = "AdminUserEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface AdminUserEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.taskmanager.karamyshev.ru/AdminUserEndpoint/lockUserRequest", output = "http://endpoint.taskmanager.karamyshev.ru/AdminUserEndpoint/lockUserResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.taskmanager.karamyshev.ru/AdminUserEndpoint/lockUser/Fault/Exception")})
    @RequestWrapper(localName = "lockUser", targetNamespace = "http://endpoint.taskmanager.karamyshev.ru/", className = "ru.karamyshev.taskmanager.endpoint.LockUser")
    @ResponseWrapper(localName = "lockUserResponse", targetNamespace = "http://endpoint.taskmanager.karamyshev.ru/", className = "ru.karamyshev.taskmanager.endpoint.LockUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.karamyshev.taskmanager.endpoint.User lockUser(
        @WebParam(name = "session", targetNamespace = "")
        ru.karamyshev.taskmanager.endpoint.Session session,
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.taskmanager.karamyshev.ru/AdminUserEndpoint/unlockUserRequest", output = "http://endpoint.taskmanager.karamyshev.ru/AdminUserEndpoint/unlockUserResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.taskmanager.karamyshev.ru/AdminUserEndpoint/unlockUser/Fault/Exception")})
    @RequestWrapper(localName = "unlockUser", targetNamespace = "http://endpoint.taskmanager.karamyshev.ru/", className = "ru.karamyshev.taskmanager.endpoint.UnlockUser")
    @ResponseWrapper(localName = "unlockUserResponse", targetNamespace = "http://endpoint.taskmanager.karamyshev.ru/", className = "ru.karamyshev.taskmanager.endpoint.UnlockUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.karamyshev.taskmanager.endpoint.User unlockUser(
        @WebParam(name = "session", targetNamespace = "")
        ru.karamyshev.taskmanager.endpoint.Session session,
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.taskmanager.karamyshev.ru/AdminUserEndpoint/removeUsByLogRequest", output = "http://endpoint.taskmanager.karamyshev.ru/AdminUserEndpoint/removeUsByLogResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.taskmanager.karamyshev.ru/AdminUserEndpoint/removeUsByLog/Fault/Exception")})
    @RequestWrapper(localName = "removeUsByLog", targetNamespace = "http://endpoint.taskmanager.karamyshev.ru/", className = "ru.karamyshev.taskmanager.endpoint.RemoveUsByLog")
    @ResponseWrapper(localName = "removeUsByLogResponse", targetNamespace = "http://endpoint.taskmanager.karamyshev.ru/", className = "ru.karamyshev.taskmanager.endpoint.RemoveUsByLogResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.karamyshev.taskmanager.endpoint.User removeUsByLog(
        @WebParam(name = "session", targetNamespace = "")
        ru.karamyshev.taskmanager.endpoint.Session session,
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.taskmanager.karamyshev.ru/AdminUserEndpoint/createUserWithRoleRequest", output = "http://endpoint.taskmanager.karamyshev.ru/AdminUserEndpoint/createUserWithRoleResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.taskmanager.karamyshev.ru/AdminUserEndpoint/createUserWithRole/Fault/Exception")})
    @RequestWrapper(localName = "createUserWithRole", targetNamespace = "http://endpoint.taskmanager.karamyshev.ru/", className = "ru.karamyshev.taskmanager.endpoint.CreateUserWithRole")
    @ResponseWrapper(localName = "createUserWithRoleResponse", targetNamespace = "http://endpoint.taskmanager.karamyshev.ru/", className = "ru.karamyshev.taskmanager.endpoint.CreateUserWithRoleResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.karamyshev.taskmanager.endpoint.User createUserWithRole(
        @WebParam(name = "session", targetNamespace = "")
        ru.karamyshev.taskmanager.endpoint.Session session,
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login,
        @WebParam(name = "password", targetNamespace = "")
        java.lang.String password,
        @WebParam(name = "role", targetNamespace = "")
        ru.karamyshev.taskmanager.endpoint.Role role
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.taskmanager.karamyshev.ru/AdminUserEndpoint/createUserWithEmailRequest", output = "http://endpoint.taskmanager.karamyshev.ru/AdminUserEndpoint/createUserWithEmailResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.taskmanager.karamyshev.ru/AdminUserEndpoint/createUserWithEmail/Fault/Exception")})
    @RequestWrapper(localName = "createUserWithEmail", targetNamespace = "http://endpoint.taskmanager.karamyshev.ru/", className = "ru.karamyshev.taskmanager.endpoint.CreateUserWithEmail")
    @ResponseWrapper(localName = "createUserWithEmailResponse", targetNamespace = "http://endpoint.taskmanager.karamyshev.ru/", className = "ru.karamyshev.taskmanager.endpoint.CreateUserWithEmailResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.karamyshev.taskmanager.endpoint.User createUserWithEmail(
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login,
        @WebParam(name = "password", targetNamespace = "")
        java.lang.String password,
        @WebParam(name = "email", targetNamespace = "")
        java.lang.String email
    ) throws Exception_Exception;
}

package ru.karamyshev.taskmanager.command.task;


import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.endpoint.Task;
import ru.karamyshev.taskmanager.util.TerminalUtil;


public class TaskUpdateByIdCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-tskupid";
    }

    @NotNull
    @Override
    public String name() {
        return "task-update-by-id";
    }

    @Override
    public @NotNull String description() {
        return "Update task by id.";
    }

    @Override
    public void execute() throws Exception {
        final Session session = serviceLocator.getSessionService().getSession();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER TASK-ID:");
        final String idTask = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdate = serviceLocator.getTaskEndpoint()
                .updateTaskById(session, idTask, name, description);
        if (taskUpdate == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}

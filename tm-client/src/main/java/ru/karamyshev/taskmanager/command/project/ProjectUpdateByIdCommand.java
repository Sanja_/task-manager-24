package ru.karamyshev.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.Exception_Exception;
import ru.karamyshev.taskmanager.endpoint.Project;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.util.TerminalUtil;


public class ProjectUpdateByIdCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-prtupid";
    }

    @NotNull
    @Override
    public String name() {
        return "project-update-by-id";
    }

    @Override
    public @NotNull String description() {
        return "Update project by id.";
    }

    @Override
    public void execute() throws Exception_Exception {
        final Session session = serviceLocator.getSessionService().getSession();
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectEndpoint().findOneProjectById(session, id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdate = serviceLocator.getProjectEndpoint().updateProjectById(session, id, name, description);
        if (projectUpdate == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}

package ru.karamyshev.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.endpoint.User;

public class ShowProfileCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-shw-prfl";
    }

    @NotNull
    @Override
    public String name() {
        return "show-profile";
    }

    @Override
    public @NotNull String description() {
        return "Show profile.";
    }

    @Override
    public void execute() throws Exception {
        final Session session = serviceLocator.getSessionService().getSession();
        User currentUser = serviceLocator.getUserEndpoint().findUserById(session);
        System.out.println("[SHOW PROFILE]");
        System.out.println("LOGIN: " + currentUser.getLogin());
        System.out.println("Role: " + currentUser.getRole());
        System.out.println("HASH PASSWORD: " + currentUser.getPasswordHash());
        System.out.println("[OK]");
    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}

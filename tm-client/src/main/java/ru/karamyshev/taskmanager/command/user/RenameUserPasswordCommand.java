package ru.karamyshev.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class RenameUserPasswordCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-rnm-psswrd";
    }

    @NotNull
    @Override
    public String name() {
        return "rename-password";
    }

    @Override
    public @NotNull String description() {
        return "Rename password account.";
    }

    @Override
    public void execute() throws Exception {
        final Session session = serviceLocator.getSessionService().getSession();
        System.out.println("CHANGE ACCOUNT PASSWORD");
        System.out.println("[ENTER OLD PASSWORD]");
        final String oldPassword = TerminalUtil.nextLine();
        System.out.println("[ENTER NEW PASSWORD]");
        final String newPassword = TerminalUtil.nextLine();
        serviceLocator.getAuthenticationEndpoint().renamePassword(session, oldPassword, newPassword);
        System.out.println("[OK]");
    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}

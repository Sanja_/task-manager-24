package ru.karamyshev.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.Project;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class ProjectRemoveByIdCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-prtrmvid";
    }

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-id";
    }

    @Override
    public @NotNull String description() {
        return "Remove project by id.";
    }

    @Override
    public void execute() throws Exception {
        final Session session = serviceLocator.getSessionService().getSession();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER PROJECT ID FOR DELETION:");
        final String idProject = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectEndpoint().removeOneProjectById(session, idProject);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}

package ru.karamyshev.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.Project;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.util.TerminalUtil;

import java.util.List;


public class ProjectRemoveByNameCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-prtrmvnm";
    }

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-name";
    }

    @Override
    public @NotNull String description() {
        return "Remove project by name.";
    }

    @Override
    public void execute() throws Exception {
        final Session session = serviceLocator.getSessionService().getSession();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER PROJECT NAME FOR DELETION:");
        final String name = TerminalUtil.nextLine();
        final List<Project> project = serviceLocator.getProjectEndpoint().removeOneProjectByName(session, name);
        if (project == null || project.isEmpty()) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}

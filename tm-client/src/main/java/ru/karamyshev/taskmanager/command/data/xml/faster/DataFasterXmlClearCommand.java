package ru.karamyshev.taskmanager.command.data.xml.faster;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;

import java.io.Serializable;

public class DataFasterXmlClearCommand extends AbstractDataCommand implements Serializable {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-fs-xml-clear";
    }

    @Override
    public @NotNull String description() {
        return "Remove xml(faster) file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[REMOVE XML(FASTER) FILE]");
        final Session session = serviceLocator.getSessionService().getSession();
        serviceLocator.getAdminEndpoint().clearDataFasterXml(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}

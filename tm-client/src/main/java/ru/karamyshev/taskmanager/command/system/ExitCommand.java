package ru.karamyshev.taskmanager.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.Session;

public class ExitCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "exit";
    }

    @Override
    public @NotNull String description() {
        return "Close application.";
    }

    @Override
    public void execute() throws Exception {
        final Session session = serviceLocator.getSessionService().getSession();
        serviceLocator.getSessionEndpoint().closeSession(session);
        serviceLocator.getSessionService().clearSession();
        System.exit(0);
    }

}

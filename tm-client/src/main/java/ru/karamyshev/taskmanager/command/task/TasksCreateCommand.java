package ru.karamyshev.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.endpoint.TaskEndpoint;
import ru.karamyshev.taskmanager.util.TerminalUtil;


public class TasksCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-tskcrt";
    }

    @NotNull
    @Override
    public String name() {
        return "task-create";
    }

    @Override
    public @NotNull String description() {
        return "Create new task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CREATE TASK");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        Session session = serviceLocator.getSessionService().getSession();
        TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        taskEndpoint.createDescription(session, name, description);
        System.out.println("[OK]");
    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }
}

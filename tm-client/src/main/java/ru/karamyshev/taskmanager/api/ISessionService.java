package ru.karamyshev.taskmanager.api;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.endpoint.Session;

public interface ISessionService {

    @Nullable
    void setSession(Session session);

    void clearSession();

    @Nullable
    Session getSession();

}

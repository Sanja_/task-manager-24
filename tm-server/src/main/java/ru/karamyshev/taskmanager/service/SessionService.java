package ru.karamyshev.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.repository.ISessionRepository;
import ru.karamyshev.taskmanager.api.service.IPropertyService;
import ru.karamyshev.taskmanager.api.service.IServiceLocator;
import ru.karamyshev.taskmanager.api.service.ISessionService;
import ru.karamyshev.taskmanager.entity.Session;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;
import ru.karamyshev.taskmanager.exception.user.AccessDeniedException;
import ru.karamyshev.taskmanager.util.HashUtil;
import ru.karamyshev.taskmanager.util.SignatureUtil;

import java.util.List;

public class SessionService extends AbstractService<Session> implements ISessionService {

    private final IServiceLocator serviceLocator;

    private final ISessionRepository sessionRepository;

    public SessionService(final ISessionRepository sessionRepository, final IServiceLocator serviceLocator) {
        super(sessionRepository);
        this.serviceLocator = serviceLocator;
        this.sessionRepository = sessionRepository;
    }

    @Override
    public void close(final Session session) throws Exception {
        validate(session);
        sessionRepository.removeByUserId(session.getUserId());
    }

    @Override
    public void closeAll(final Session session) throws Exception {
        validate(session);
        sessionRepository.clear(session.getUserId());
    }

    @Nullable
    @Override
    public User getUser(final Session session) throws Exception {
        final String userId = getUserId(session);
        return serviceLocator.getUserService().findById(userId);
    }

    @Nullable
    @Override
    public String getUserId(final Session session) throws Exception {
        validate(session);
        return session.getUserId();
    }

    @Nullable
    @Override
    public List<Session> getListSession(final Session session) throws Exception {
        validate(session);
        return sessionRepository.findAllSession(session.getUserId());
    }

    @Nullable
    @Override
    public Session sign(final Session session) {
        if (session == null) return null;
        session.setSignature(null);
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final String salt = propertyService.getSessionSalt();
        @NotNull final Integer cycle = propertyService.getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        return session;
    }

    @Override
    public boolean isValid(final Session session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void validate(@Nullable final Session session) throws Exception {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        final String signatureSource = session.getSignature();
        final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        if (!sessionRepository.contains(session.getUserId())) throw new AccessDeniedException();
    }

    @Override
    public void validate(@Nullable final Session session, @Nullable final Role role) throws Exception {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        final String userId = session.getUserId();
        final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new AccessDeniedException();
        if (user.getRole() == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Nullable
    @Override
    public Session open(@Nullable final String login, @Nullable final String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check) return null;
        final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) return null;
        final Session session = new Session();
        session.setUserId(Long.toString(user.getId()));
        session.setTimestamp(System.currentTimeMillis());
        sessionRepository.add(session);
        return sign(session);
    }

    @Override
    public boolean checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) return false;
        final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    public void signOutByLogin(final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new AccessDeniedException();
        final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final String userId = Long.toString(user.getId());
        sessionRepository.removeByUserId(userId);
    }

    @Override
    public void signOutByUserId(final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        sessionRepository.removeByUserId(userId);
    }

}

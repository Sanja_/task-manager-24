package ru.karamyshev.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.api.endpoint.IAdminEndpoint;
import ru.karamyshev.taskmanager.api.service.IServiceLocator;
import ru.karamyshev.taskmanager.dto.Fail;
import ru.karamyshev.taskmanager.dto.Result;
import ru.karamyshev.taskmanager.dto.Success;
import ru.karamyshev.taskmanager.entity.Session;
import ru.karamyshev.taskmanager.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class AdminEndpoint implements IAdminEndpoint {

    private IServiceLocator serviceLocator;

    public AdminEndpoint() {
    }

    public AdminEndpoint(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    @WebMethod
    public Result saveDataBinary(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().saveBinary();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result loadDataBinary(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);

        try {
            serviceLocator.getAdminService().loadBinary();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearDataBinary(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);

        try {
            serviceLocator.getAdminService().clearBinary();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result saveDataBase64(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().saveBinary();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result loadDataBase64(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().loadBase64();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearDataBase64(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().clearBase64();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result saveDataFasterJson(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().saveFasterJson();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result loadDataFasterJson(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().loadFasterJson();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearDataFasterJson(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().clearFasterJson();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result saveDataFasterXml(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().saveFasterXml();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result loadDataFasterXml(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().loadFasterXml();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearDataFasterXml(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().clearFasterXml();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result saveDataJaxBJson(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().saveJaxBJson();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result loadDataJaxBJson(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().loadJaxBJson();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearDataJaxBJson(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().clearJaxBJson();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result saveDataJaxBXml(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().saveJaxBXml();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result loadDataJaxBXml(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().loadJaxBXml();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearDataJaxBXml(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().clearJaxBXml();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

}

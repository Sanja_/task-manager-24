package ru.karamyshev.taskmanager.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.endpoint.IAdminUserEndpoint;
import ru.karamyshev.taskmanager.api.service.IServiceLocator;
import ru.karamyshev.taskmanager.entity.Session;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class AdminUserEndpoint implements IAdminUserEndpoint {

    private IServiceLocator serviceLocator;

    public AdminUserEndpoint() {
    }

    public AdminUserEndpoint(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    @WebMethod
    public User createUserWithEmail(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "email", partName = "email") @Nullable final String email
    ) throws Exception {
        return serviceLocator.getUserService().create(login, password, email);

    }

    @Nullable
    @Override
    @WebMethod
    public User createUserWithRole(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "login", partName = "login") final String login,
            @WebParam(name = "password", partName = "password") final String password,
            @WebParam(name = "role", partName = "role") final Role role
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().create(login, password, role);
    }

    @Nullable
    @Override
    @WebMethod
    public User lockUser(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "login", partName = "login") final String login
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        final String currentLogin = serviceLocator.getAuthenticationService().getCurrentLogin();
        return serviceLocator.getUserService().lockUserByLogin(login, currentLogin);
    }

    @Nullable
    @Override
    @WebMethod
    public User unlockUser(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "login", partName = "login") final String login
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().unlockUserByLogin(login);
    }

    @Nullable
    @Override
    @WebMethod
    public User removeUsByLog(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "login", partName = "login") final String login
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().removeByLogin(login);
    }

}

package ru.karamyshev.taskmanager.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.endpoint.IProjectEndpoint;
import ru.karamyshev.taskmanager.api.service.IServiceLocator;
import ru.karamyshev.taskmanager.entity.Project;
import ru.karamyshev.taskmanager.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint implements IProjectEndpoint {

    public ProjectEndpoint() {
    }

    private IServiceLocator serviceLocator;

    public ProjectEndpoint(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public void createNameProject(
            @WebParam (name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().create(session.getUserId(), name);
    }


    @Override
    @WebMethod
    public void addProject(
            @WebParam (name = "session", partName = "session") @Nullable Session session,
            @WebParam (name = "project", partName = "project") @Nullable Project project
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().add(session.getUserId(), project);
    }

    @Override
    @WebMethod
    public void removeProject(
            @WebParam (name = "session", partName = "session") @Nullable Session session,
            @WebParam (name = "project", partName = "project") @Nullable Project project
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().remove(session.getUserId(), project);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Project> findAllProject(
            @WebParam (name = "session", partName = "session") @Nullable Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAll(session.getUserId());
    }

    @Override
    @WebMethod
    public void clearProject(
            @WebParam (name = "session", partName = "session") @Nullable Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().clear(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public Project findOneProjectByIndex(
            @WebParam (name = "session", partName = "session") @Nullable Session session,
            @WebParam (name = "projectIndex", partName = "projectIndex") @Nullable Integer index
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findOneByIndex(session.getUserId(), index);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Project> findOneProjectByName(
            @WebParam (name = "session", partName = "session") @Nullable Session session,
            @WebParam (name = "projectName", partName = "projectName") @Nullable String name
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findOneByName(session.getUserId(), name);
    }

    @Nullable
    @Override
    @WebMethod
    public Project updateProjectById(
            @WebParam (name = "session", partName = "session") @Nullable Session session,
            @WebParam (name = "projectId", partName = "projectId" ) @Nullable String id,
            @WebParam (name = "projectUpName", partName = "projectUpName") @Nullable String name,
            @WebParam (name = "projectUpDescription", partName = "projectUpDescription") @Nullable String description
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().updateProjectById(session.getUserId(), id, name, description);
    }

    @Nullable
    @Override
    @WebMethod
    public Project removeOneProjectByIndex(
            @WebParam (name = "session", partName = "session") @Nullable Session session,
            @WebParam (name = "projectIndex", partName = "projectIndex") @Nullable Integer index
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeOneByIndex(session.getUserId(), index);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Project> removeOneProjectByName(
            @WebParam (name = "session", partName = "session") @Nullable Session session,
            @WebParam (name = "projectName", partName = "projectName") @Nullable String name
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeOneByName(session.getUserId(), name);
    }

    @Nullable
    @Override
    @WebMethod
    public Project findOneProjectById(
            @WebParam (name = "session", partName = "session") @Nullable Session session,
            @WebParam (name = "projectId", partName = "projectId") @Nullable String id
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findOneById(session.getUserId(), id);
    }

    @Nullable
    @Override
    @WebMethod
    public Project removeOneProjectById(
            @WebParam (name = "session", partName = "session") @Nullable Session session,
            @WebParam (name = "projectId", partName = "projectId") @Nullable String id
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeOneById(session.getUserId(), id);
    }

    @Nullable
    @Override
    @WebMethod
    public Project updateProjectByIndex(
            @WebParam (name = "session", partName = "session") @Nullable Session session,
            @WebParam (name = "projectIndex", partName = "projectIndex") @Nullable Integer index,
            @WebParam (name = "projectName", partName = "projectName") @Nullable String name,
            @WebParam (name = "projectDescription", partName = "projectDescription") @Nullable String description
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().updateProjectByIndex(session.getUserId(), index, name, description);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Project> getProjectList(
            @WebParam (name = "session", partName = "session") @Nullable Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().getList();
    }

    @Override
    @WebMethod
    public void loadProject(
            @WebParam (name = "session", partName = "session") @Nullable Session session,
            @WebParam (name = "projects", partName ="projects") @Nullable List<Project> projects
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().load(projects);
    }

    @Override
    @WebMethod
    public void createDescriptionProject(
            @WebParam (name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name,
            @WebParam(name = "description", partName = "description") @Nullable String description
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().create(session.getUserId(), name, description);
    }

}

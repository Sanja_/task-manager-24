package ru.karamyshev.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;

public interface IServiceLocator {

    @NotNull
    IUserService getUserService();

    @NotNull
    IAuthenticationService getAuthenticationService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IAdminService getAdminService();

}
